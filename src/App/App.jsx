import React from 'react';
import {Router, Route} from 'react-router-dom';
import {connect} from 'react-redux';

import {history} from '../_helpers';
import {alertActions} from '../_actions';
import {PrivateRoute} from '../_components';
import {AllPostPage} from '../PostPage';
import {LoginPage} from '../LoginPage';

class App extends React.Component {
    constructor(props) {
        super(props);

        const {dispatch} = this.props;
        history.listen((location, action) => {
            dispatch(alertActions.clear());
        });
    }

    render() {
        const {alert} = this.props;
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-8 col-sm-offset-2">
                        <Router history={history}>
                            <div>
                                <PrivateRoute exact path="/" component={AllPostPage}/>
                                <Route path="/login" component={LoginPage}/>
                            </div>
                        </Router>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-4 col-sm-offset-4">
                        {alert.message &&
                        <div className={`alert ${alert.type}`} role="alert">
                            {alert.message}
                        </div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const {alert} = state;
    return {
        alert
    };
}

const connectedApp = connect(mapStateToProps)(App);
export {connectedApp as App};