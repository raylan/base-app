import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { posts } from './posts.reducer';
import { alert } from './alert.reducer';

const rootReducer = combineReducers({
  authentication,
  posts,
  alert
});

export default rootReducer;