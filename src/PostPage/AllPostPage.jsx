import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import './styles.css';

import { postActions } from '../_actions';

class AllPostPage extends React.Component {
    componentDidMount() {
        this.props.dispatch(postActions.getAll());
    }

    render() {
        const { posts } = this.props;
        return (
            <div className="col-md-12">
                <h3>Posts</h3>
                {posts.loading && <em>Loading posts...</em>}
                {posts.error && <span className="text-danger">ERROR: {posts.error}</span>}
                {posts.items &&
                    <table className="table table-striped table-hovered">
                        <thead>
                            <tr>
                                <th className="col-sm-1">Id</th>
                                <th className="col-sm-9">Title</th>
                                <th className="col-sm-2 text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        {posts.items.posts.map(post =>
                            <tr key={post.id}>
                                <td>{post.id}</td>
                                <td>{post.title}</td>
                                <td className="text-center">
                                    <button className="btn btn-sm btn-primary">
                                        <i className="glyphicon glyphicon-pencil"> </i>
                                    </button>
                                    <button className="btn btn-sm btn-danger">
                                        <i className="glyphicon glyphicon-trash"> </i>
                                    </button>
                                </td>
                            </tr>
                        )}
                        </tbody>
                    </table>
                }
                <p>
                    <Link to="/login">Logout</Link>
                </p>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { posts, authentication } = state;
    const { post } = authentication;
    return {
        post,
        posts
    };
}

const connectedAllPostPage = connect(mapStateToProps)(AllPostPage);
export { connectedAllPostPage as AllPostPage };