## Base APP

#### Prerequisites:

- Git
- Nodejs

#### Instructions

- Open your terminal and type the following commands:

  - git clone https://gitlab.com/raylan/base-app.git base-app
  - cd base-app
  - npm install
  - npm start

- Finished!

#### Additional Information

- The default API Url is "http://localhost/api". Edit the "apiUrl" in "webpack.config.js" if you need to change this
